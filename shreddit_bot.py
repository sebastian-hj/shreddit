# -*- coding: utf-8 -*-

import sys
import argparse
import json
import logging
from datetime import datetime, time
from time import sleep
import telegram
from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters
import praw


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

# Global Variables
python_version = 0
bot_valid_chat_ids = list()
quotes_lines = list()
counter = 0
scheduler_dict = {'time_list': list(), 'number_of_messages': 0}


##################
# System functions
##################
def set_python_version():
    global python_version

    if sys.version_info.major == 3:
        python_version = 3
    else:
        python_version = 2


###########
# Files I/O
###########
def load_json_to_dict(file_path):
    try:
        with open(file_path, 'r') as f_id:
            json_dict = json.load(f_id)
    except IOError:
        print('ERROR: Cannot open file \'{0}\''.format(file_path))
        json_dict = ''
    except ValueError:
        print('ERROR: Is \'{0}\' a JSON file?'.format(file_path))
        json_dict = ''

    return json_dict


##################
# Reddit functions
##################
def validate_reddit_api_config(config_dict):
    main_field = 'reddit'
    if main_field not in config_dict:
        sys.exit('ERROR: No \'{0}\' field in JSON file!'.format(main_field))
    else:
        reddit_api_fields = ['client_id', 'client_secret', 'username', 'password', 'user_agent', 'subreddit']
        for field in reddit_api_fields:
            if field not in config_dict[main_field]:
                sys.exit('ERROR: No \'{0}\' subfield in \'{1}\' field!'.format(field, main_field))
            elif not config_dict[main_field][field]:
                sys.exit('ERROR: \'{0}\' subfield is empty!'.format(field))


def create_reddit_message():
    # Message
    message_list = list()

    # Iterate over subreddits specified in JSON
    for element in config_dict['reddit']['subreddit']:
        # Get variables for current subreddit
        subreddit_name = element['name']
        subreddit_type = element['type']
        subreddit_number_of_posts = element['posts']

        # Get posts for subreddit type
        if subreddit_type == 'hot':
            subreddit = reddit.subreddit(subreddit_name).hot()
        elif subreddit_type == 'new':
            subreddit = reddit.subreddit(subreddit_name).new()
        elif subreddit_type == 'controversial':
            subreddit = reddit.subreddit(subreddit_name).controversial()
        elif subreddit_type == 'top':
            subreddit = reddit.subreddit(subreddit_name).top()
        elif subreddit_type == 'rising':
            subreddit = reddit.subreddit(subreddit_name).rising()
        else:
            subreddit = None
            message_list.append('Sorry, \'{0}\' type not yet supported...'.format(subreddit_type))

        if subreddit is not None:
            # Initialize counter
            counter = 0
            # Get posts based on number of posts
            while counter < subreddit_number_of_posts:
                # Get submission if it is not stickied
                submission = next(s for s in subreddit if not s.stickied)
                # Get author name
                author = submission.author.name if submission.author is not None else 'deleted?'
                # Get submission title
                title = submission.title
                # Get submission link
                link = submission.shortlink
                # Get media. May or may not be multiple files
                media = list()
                for image in submission.preview['images']:
                    media.append(image['source']['url'])
                # Print information
                message_list.append('\'{0}\' by /r/{1}: {2} ({3})\n'.format(title, author, media, link))
                # Increase counter
                counter += 1

    return message_list


################
# Time functions
################
def get_timestamp():
    return datetime.now().strftime('%Y%m%d%A_%H%M%S')


def get_hour():
    return int(datetime.now().strftime('%H'))


#####################
# Scheduler functions
#####################
def configure_scheduler(config_dict):
    global scheduler_dict

    is_config_ok = False
    if 'schedule' in config_dict['bot']:
        if 'frequency' in config_dict['bot']['schedule'] and config_dict['bot']['schedule']['frequency'] == 'daily':
            if 'time' in config_dict['bot']['schedule']:
                # Convert to 'datetime.time' format
                scheduler_dict['time_list'] = [time(t) for t in config_dict['bot']['schedule']['time']]
                # validate_scheduler_time()
                is_config_ok = True
            if 'number_of_messages' in config_dict['bot']['schedule']:
                scheduler_dict['number_of_messages'] = config_dict['bot']['schedule']['number_of_messages']
    else:
        scheduler_dict['time_list'] = ''
        scheduler_dict['number_of_messages'] = 0
        is_config_ok = False

    return is_config_ok


########################
# Telegram Bot functions
########################
def validate_bot_config(config_dict):
    if 'bot' not in config_dict:
        sys.exit('ERROR: No \'bot\' field in JSON file!')
    else:
        bot_fields = ['token', 'username', 'first_name', 'valid_chat_ids']
        for field in bot_fields:
            if field not in config_dict['bot']:
                sys.exit('ERROR: No \'{0}\' subfield in \'bot\' field!'.format(field))
            elif not config_dict['bot'][field]:
                sys.exit('ERROR: \'{0}\' subfield is empty!'.format(field))


def validate_chat_id(bot, update):
    # Get message and user
    if python_version == 3:
        message = update.message.text
        user = update.message.chat.first_name
    else:
        message = update.message.text.encode('utf-8')
        user = update.message.chat.first_name.encode('utf-8')

    print('[{0}] Received message from \'{1}\': {2}'.format(get_timestamp(), user, message))

    if update.message.chat_id in bot_valid_chat_ids:
        return True
    else:
        bot.send_message(chat_id=update.message.chat_id, text='Unauthorized user!')
        return False


def send_message(bot, update, text):
    bot.send_message(chat_id=update.message.chat_id, text=text)


def command_start(bot, update):
    if validate_chat_id(bot, update) is True:
        send_message(bot, update, 'Soy El Estro bot!\n'
                                  '/quote: send quote (beta)')


def command_reddit(bot, update):
    # Get chat ID
    chat_id = bot_valid_chat_ids[0]

    # Show 'typing' action
    bot.send_chat_action(chat_id=chat_id, action=telegram.ChatAction.TYPING)

    # Get reddit message list
    message_list = create_reddit_message()

    for message in message_list:
        # Show 'typing' action
        bot.send_chat_action(chat_id=chat_id, action=telegram.ChatAction.TYPING)
        # Send reddit message
        bot.send_message(chat_id=chat_id, text=message)
        # Stop for half a second
        sleep(0.5)

    # Print to console
    print('[{0}] Sent message to \'{1}\''.format(get_timestamp(), chat_id))


def command_echo(bot, update):
    if validate_chat_id(bot, update) is True:
        send_message(bot, update, 'Sorry. Cannot hold a conversation yet...\n¯\_(ツ)_/¯')


def command_unknown(bot, update):
    if validate_chat_id(bot, update) is True:
        send_message(bot, update, 'Invalid command!')


if __name__ == '__main__':
    # Create parser
    parser = argparse.ArgumentParser(description='shReddit: Reddit parser.')
    parser.add_argument('config_json', help='Path to configuration JSON', type=str)
    # Get parser results
    args = parser.parse_args()

    # Set Python version
    set_python_version()

    # Open JSON file to dict
    config_dict = load_json_to_dict(args.config_json)
    if not config_dict:
        sys.exit('ERROR opening configuration JSON')

    # Validate reddit API configuration
    validate_reddit_api_config(config_dict)

    # Start a reddit instance
    reddit = praw.Reddit(client_id=config_dict['reddit']['client_id'],
                         client_secret=config_dict['reddit']['client_secret'],
                         username=config_dict['reddit']['username'],
                         password=config_dict['reddit']['password'],
                         user_agent=config_dict['reddit']['user_agent'])

    # Validate bot configuration
    validate_bot_config(config_dict)

    # Get token, username, and first name
    bot_token = config_dict['bot']['token']
    bot_username = config_dict['bot']['username']
    bot_first_name = config_dict['bot']['first_name']
    # Get valid chat IDs
    bot_valid_chat_ids = config_dict['bot']['valid_chat_ids']

    # Create instanse of Telegram Bot
    bot = telegram.Bot(token=bot_token)

    # Check if credentials are correct
    if bot_username != bot.username or bot_first_name != bot.first_name:
        sys.exit('ERROR: Bot name does not match...')

    # Create 'Updater' object
    updater = Updater(token=bot_token)
    # Create 'Dispatcher' object
    dispatcher = updater.dispatcher

    # Use 'CommandHandler' subclass to call 'start' function called every time the Bot receives a Telegram message that
    # contains the '/start' command
    start_handler = CommandHandler('start', command_start)
    dispatcher.add_handler(start_handler)

    # Like above but for '/reddit'
    reddit_handler = CommandHandler('reddit', command_reddit)
    dispatcher.add_handler(reddit_handler)

    # Reply to any message
    echo_handler = MessageHandler(Filters.text, command_echo)
    dispatcher.add_handler(echo_handler)

    # Unknown handler for unknown commands
    unknown_handler = MessageHandler(Filters.command, command_unknown)
    dispatcher.add_handler(unknown_handler)

    # Configure scheduler
    if configure_scheduler(config_dict):
        # Create JobQueue
        job = updater.job_queue
        # Create daily functions
        for time_value in scheduler_dict['time_list']:
            job.run_daily(command_reddit, time=time_value)

    # Start bot
    # Settings from: https://github.com/python-telegram-bot/python-telegram-bot/issues/1022
    updater.start_polling(poll_interval=10, clean=True, bootstrap_retries=10, timeout=20)
    updater.idle()
