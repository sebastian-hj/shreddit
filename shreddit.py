import sys
import argparse
import json
import praw


def load_json_to_dict(file_path):
    try:
        with open(file_path, 'r') as f_id:
            json_dict = json.load(f_id)
    except IOError:
        print('ERROR: Cannot open file \'{0}\''.format(file_path))
        json_dict = ''
    except ValueError:
        print('ERROR: Is \'{0}\' a JSON file?'.format(file_path))
        json_dict = ''

    return json_dict


def validate_reddit_api_config(config_dict):
    main_field = 'reddit'
    if main_field not in config_dict:
        sys.exit('ERROR: No \'{0}\' field in JSON file!'.format(main_field))
    else:
        reddit_api_fields = ['client_id', 'client_secret', 'username', 'password', 'user_agent', 'subreddit']
        for field in reddit_api_fields:
            if field not in config_dict[main_field]:
                sys.exit('ERROR: No \'{0}\' subfield in \'{1}\' field!'.format(field, main_field))
            elif not config_dict[main_field][field]:
                sys.exit('ERROR: \'{0}\' subfield is empty!'.format(field))


if __name__ == '__main__':
    # Create parser
    parser = argparse.ArgumentParser(description='shReddit: Reddit parser.')
    parser.add_argument('config_json', help='Path to configuration JSON', type=str)
    # Get parser results
    args = parser.parse_args()

    # Open JSON file to dict
    config_dict = load_json_to_dict(args.config_json)
    if not config_dict:
        sys.exit('ERROR opening configuration JSON')

    # Validate reddit API configuration
    validate_reddit_api_config(config_dict)

    # Start a reddit instance
    reddit = praw.Reddit(client_id=config_dict['reddit']['client_id'],
                         client_secret=config_dict['reddit']['client_secret'],
                         username=config_dict['reddit']['username'],
                         password=config_dict['reddit']['password'],
                         user_agent=config_dict['reddit']['user_agent'])

    # Iterate over subreddits specified in JSON
    for element in config_dict['reddit']['subreddit']:
        # Get variables for current subreddit
        subreddit_name = element['name']
        subreddit_type = element['type']
        subreddit_number_of_posts = element['posts']

        # Get posts for subreddit type
        if subreddit_type == 'hot':
            subreddit = reddit.subreddit(subreddit_name).hot()
        elif subreddit_type == 'new':
            subreddit = reddit.subreddit(subreddit_name).new()
        elif subreddit_type == 'controversial':
            subreddit = reddit.subreddit(subreddit_name).controversial()
        elif subreddit_type == 'top':
            subreddit = reddit.subreddit(subreddit_name).top()
        elif subreddit_type == 'rising':
            subreddit = reddit.subreddit(subreddit_name).rising()
        else:
            subreddit = None
            print('Sorry, \'{0}\' type not yet supported...'.format(subreddit_type))

        if subreddit is not None:
            # Initialize counter
            counter = 0
            # Get posts based on number of posts
            while counter < subreddit_number_of_posts:
                # Get submission if it is not stickied
                submission = next(s for s in subreddit if not s.stickied)
                # Get author name
                author = submission.author.name if submission.author is not None else 'deleted?'
                # Get submission title
                title = submission.title
                # Get submission link
                link = submission.shortlink
                # Get media. May or may not be multiple files
                media = list()
                for image in submission.preview['images']:
                    media.append(image['source']['url'])
                # Print information
                print('\'{0}\' by /r/{1}: {2} ({3})'.format(title, author, media, link))
                # Increase counter
                counter += 1
